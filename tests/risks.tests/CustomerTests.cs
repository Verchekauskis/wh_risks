﻿using System.Collections.Generic;
using FluentAssertions;
using risks.Services;
using Xunit;

namespace risks.tests
{
    public class CustomerTests
    {
        [Fact]
        public void WinningRate_NoBets()
        {
            // Arrange
            var customer = new Customer {Bets = new List<SettledBet>()};

            // Assert
            customer.WinningRate
                .Should()
                .Be(0);
        }

        [Fact]
        public void WinningRate_Bets_Null()
        {
            // Arrange
            var customer = new Customer{Bets = null};

            // Assert
            customer.WinningRate
                .Should()
                .Be(0);
        }

        [Fact]
        public void WinningRate_Is_Calculated()
        {
            // Arrange
            var bets = new List<SettledBet>
            {
                new SettledBet {CustomerId = 1, Win = 1},
                new SettledBet {CustomerId = 1, Win = 0},
                new SettledBet {CustomerId = 1, Win = 1},
                new SettledBet {CustomerId = 1, Win = 0}
            };
            var customer = new Customer{CustomerId = 1, Bets = bets};

            // Assert
            customer.WinningRate
                .Should()
                .Be(0.5);
        }

        [Fact]
        public void AverageStake_Is_Calculated()
        {
            // Arrange
            var bets = new List<SettledBet>
            {
                new SettledBet {CustomerId = 1, Stake = 10},
                new SettledBet {CustomerId = 1, Stake = 20},
                new SettledBet {CustomerId = 1, Stake = 20},
                new SettledBet {CustomerId = 1, Stake = 10}
            };
            var customer = new Customer { CustomerId = 1, Bets = bets };

            // Assert
            customer.AverageStake
                .Should()
                .Be(15);
        }
    }
}
