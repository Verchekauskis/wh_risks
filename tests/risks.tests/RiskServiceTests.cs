﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using risks.Services;
using Xunit;

namespace risks.tests
{
    public class RiskServiceTests
    {
        [Fact]
        public async Task Customer_With_High_Risk_Should_Be_Identified()
        {
            // Arrange
            var bets = new List<SettledBet>
            {
                new SettledBet{CustomerId = 1, Win = 1},
                new SettledBet{CustomerId = 1, Win = 0},
                new SettledBet{CustomerId = 1, Win = 2},
                new SettledBet{CustomerId = 2, Win = 2},
                new SettledBet{CustomerId = 2, Win = 0}
            };

            var mockBetsProvider = new Mock<IBetsProvider>();
            mockBetsProvider.Setup(x => x.Settled()).Returns(Task.FromResult(bets));

            var subject = new RiskService(mockBetsProvider.Object);

            // Act
            var riskytCustomers = await subject.IdentifyRiskyCustomers();

            // Assert
            riskytCustomers
                .Should().HaveCount(1, "Customer 1 requires attention");
            riskytCustomers.First().CustomerId
                .Should().Be(1, "Customer 1 requires attention");

        }
    }
}
