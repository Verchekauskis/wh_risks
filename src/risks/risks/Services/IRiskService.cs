﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace risks.Services
{
    public interface IRiskService
    {
        Task<List<Customer>> IdentifyRiskyCustomers();
        Task<List<UnsettledBet>> IdentifyRiskyBets();
    }

    public class RiskService : IRiskService
    {
        private readonly IBetsProvider _betsProvider;

        public RiskService(IBetsProvider betsProvider)
        {
            _betsProvider = betsProvider;
        }

        public async Task<List<Customer>> IdentifyRiskyCustomers()
        {
            var bets = await _betsProvider.Settled().ConfigureAwait(false);
            return  bets.GroupBy(b => b.CustomerId, b => b, 
                (key, g) => new Customer
                {
                    CustomerId = key,
                    Bets = g.ToList()
                })
                .Where(x => x.WinningRate > 0.6)
                .ToList();
        }

        public async Task<List<UnsettledBet>> IdentifyRiskyBets()
        {
            var riskyBets = from bet in await _betsProvider.Unsettled()
                join customer in await IdentifyRiskyCustomers() on bet.CustomerId equals customer.CustomerId
                select new UnsettledBet
                {
                    CustomerId = bet.CustomerId,
                    Event = bet.Event,
                    Stake = bet.Stake,
                    ToWin = bet.ToWin,
                    Participant = bet.Participant,
                    RiskValue = bet.Stake > 30 * customer.AverageStake
                        ? RiskValue.HighlyUnusual
                        : bet.Stake > 10 * customer.AverageStake
                            ? RiskValue.Unusual
                            : bet.ToWin > 1000
                                ? RiskValue.LargeAmount
                                : customer.WinningRate > 0.6
                                    ? RiskValue.Risky
                                    : RiskValue.Usual

                };
            return riskyBets.ToList();
        }
    }

    public class Customer
    {
        public int CustomerId { get; set; }
        public List<SettledBet> Bets { get; set; }
        public double WinningRate =>
             Bets!=null && Bets.Count > 0
                ? (double)Bets.Count(x => x.Win > 0) / Bets.Count
                : 0;

        public double AverageStake =>
            Bets != null && Bets.Count > 0
                ? (double)Bets.Average(x => x.Stake)
                : 0;
    }
}