﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace risks.Services
{
    public interface IBetsProvider
    {
        Task<List<SettledBet>> Settled();
        Task<List<UnsettledBet>> Unsettled();
    }

    public class SettledBet : Bet
    {
        public double Win { get; set; }
    }

    public class UnsettledBet : Bet
    {
        public double ToWin { get; set; }
        public RiskValue RiskValue { get; set; }
    }

    public class Bet
    {
        [JsonProperty("customer")]
        public int CustomerId { get; set; }
        public double Stake { get; set; }
        public int Event { get; set; }
        public int Participant { get; set; }
    }

    public enum RiskValue
    {
        Risky,
        Unusual,
        HighlyUnusual,
        LargeAmount,
        Usual
    }

}