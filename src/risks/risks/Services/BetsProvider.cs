﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace risks.Services
{
    public class BetsProvider : IBetsProvider
    {
        private readonly IBetHttpClient _httpClient;

        public BetsProvider(IBetHttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public Task<List<SettledBet>> Settled()
        {
            const string url = "https://private-anon-c2857211a9-whbetsapi.apiary-mock.com/settled";
            return _httpClient.GetAsync<List<SettledBet>>(url);
        }

        public Task<List<UnsettledBet>> Unsettled()
        {
            const string url = "https://private-anon-c2857211a9-whbetsapi.apiary-mock.com/unsettled";
            return _httpClient.GetAsync<List<UnsettledBet>>(url);
        }
    }
}
