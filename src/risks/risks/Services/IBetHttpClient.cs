﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace risks.Services
{
    public interface IBetHttpClient
    {
        Task<T> GetAsync<T>(string url);
    }

    public class BetHttpClient : IBetHttpClient
    { 
        private static readonly HttpClient HttpClient = new HttpClient();

        public async Task<T> GetAsync<T>(string url)
        {
            var response = await HttpClient.GetAsync(url).ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<T>(content);
            }
            throw new Exception(content);
        }
    }
}
