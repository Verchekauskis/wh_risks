﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using risks.Services;

namespace risks.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRiskService _riskService;

        public HomeController(IRiskService riskService)
        {
            _riskService = riskService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> RiskyCustomersPartial()
        {
            var riskyCustomers = await _riskService.IdentifyRiskyCustomers();
            return PartialView("RiskyCustomers", riskyCustomers);
        }

        public async Task<IActionResult> RiskyBetsPartial()
        {
            var riskyBets = await _riskService.IdentifyRiskyBets();
            return PartialView("RiskyBets", riskyBets);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
