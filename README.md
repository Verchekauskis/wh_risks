# README #

This project implementation of simple risks application using to monitor customers and bets


### Prerequisites ###

* .netCore 1.1
* VisualStudio 2017 (to Debug)


### How to run ###
It is possible to run project from VisualStudio or console

* From VS - open project in VisualStudio and press run
* From console - open CMD and navigate to folder where csproj file is located and type command ``` dotnet run ```


### What can be done else ###

* Add more tests to cover functionality
* Load data from client and auto refresh
* Apply different styles to different risk values
* Configurable sources of bets
* Performance optimizations
* Exception and error handling
* Clean up of default pages